﻿CREATE PROC uspEmployeeDemo
(
	@Emp_Name Varchar(55),
	@Emp_Sal Decimal(10,2)
)
AS
	
	BEGIN TRANSACTION ED

	DECLARE @Error Varchar(MAX)

		BEGIN TRY
			
			IF @Emp_Sal<=2000
				BEGIN
					
					INSERT INTO Employee_Demo
					(
						Emp_Name,
						Emp_Sal
					)
					VALUES
					(
						@Emp_Name,
						@Emp_Sal
					)

					COMMIT TRANSACTION ED

				END
			ELSE
				BEGIN
					--ROLLBACK TRANSACTION ED
					RAISERROR('Enter employee salary less than 2000.',16,1)
				END

		END TRY

		BEGIN CATCH
			SET @Error=CONCAT(ERROR_LINE(),'|',ERROR_MESSAGE(),'|',ERROR_NUMBER(),'|',ERROR_PROCEDURE())
			ROLLBACK TRANSACTION ED
			RAISERROR(@Error,16,1)
		END CATCH 

