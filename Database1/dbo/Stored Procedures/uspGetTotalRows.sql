﻿CREATE PROCEDURE uspGetTotalRows
(
	@TableName varchar(MAX)
)
AS
	BEGIN
		
		--DECLARE @TableName Varchar(MAX)
		DECLARE @dynamicQuery NVarchar(MAX)
		DECLARE @totalRowsCount int

		--SET @TableName='tblEmployee'

		SET @dynamicQuery=
			CONCAT('SELECT @totalRowsCount=COUNT(*)',
			' FROM ',
			@TableName
			)

		DECLARE @param Nvarchar(MAX)
			SET @param='@totalRowsCount int OUT'

		EXEC sp_executesql @dynamicQuery,@param,@totalRowsCount=@totalRowsCount OUT

		RETURN @totalRowsCount

	END
