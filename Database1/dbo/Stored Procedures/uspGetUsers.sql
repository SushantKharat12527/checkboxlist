﻿CREATE PROCEDURE [dbo].[uspGetUsers]
	@Command Varchar(100)=NULL,

	@UserId Numeric(18,0)


AS
	
		IF @Command='UserData'
			BEGIN
				SELECT 
					U.UserId,
					U.FirstName,
					U.LastName
					FROM tblUsers AS U
						WHERE U.UserId=@UserId
			END
		ELSE IF @Command='UserHobbyData'
			BEGIN
				SELECT 
					UH.HobbyId,
					UH.HobbyName,
					UH.Intrested
					FROM tblUserHobby AS UH
						WHERE UH.UserId=@UserId
			END

GO
