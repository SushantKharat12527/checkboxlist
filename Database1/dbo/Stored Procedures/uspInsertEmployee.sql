﻿CREATE Proc uspInsertEmployee
(
	@FirstName Varchar(50),
	@LastName varchar(50)
)
AS

	BEGIN

		BEGIN TRANSACTION TranEmployee

		BEGIN TRY 
				
				INSERT INTO tblEmployee
				(
					FirstName,
					LastName
				)
				VALUES
				(
					@FirstName,
					@LastName
				)

				--DECLARE @value int
				--	SET @value=2;

				--SELECT @value/0

				COMMIT TRANSACTION TranEmployee

		END TRY

		BEGIN CATCH
			ROLLBACK TRANSACTION TranEmployee
			DECLARE @ErrorMessage Varchar(MAX)=ERROR_MESSAGE()
			RAISERROR(@ErrorMessage,16,1)
		END CATCH

	END 
