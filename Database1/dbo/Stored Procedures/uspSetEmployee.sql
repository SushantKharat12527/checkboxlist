﻿CREATE PROCEDURE [dbo].[uspSetEmployee]
	@Command Varchar(50)=NULL,

	@EmployeeId Numeric(18,0)=NULL,
	@FirstName Varchar(50)=NULL,
	@LastName varchar(50)=NULL,
	@IsTerminate bit=null, 

	@Status int OUT,
	@Message varchar(MAX) OUT
AS

	BEGIN
		
			DECLARE @ErrorMessage Varchar(MAX)

			IF @Command='Insert'
				BEGIN
					
					BEGIN TRANSACTION

						BEGIN TRY 
							
								INSERT INTO tblEmployees
								(
									FirstName,
									LastName,
									IsTerminate
								)
								VALUES
								(
									@FirstName,
									@LastName,
									@IsTerminate
								)
						
								SET @Status=1
								SET @Message='Insert Sucessfully'

								COMMIT TRANSACTION

						END TRY

						BEGIN CATCH
							SET @Status=0
							SET @ErrorMessage=ERROR_MESSAGE()
							ROLLBACK TRANSACTION
							RAISERROR(@ErrorMessage,16,1)
						END CATCH
				END


	END