﻿CREATE PROCEDURE [dbo].[uspSetUserCommunication]
	
	@Command Varchar(50)=NULL,
	
	@UserId Numeric(18,0)=NULL,
	@MobileNo Varchar(50)=NULL,
	@EmailId Varchar(50)=NULL

AS
BEGIN 
		
		DECLARE @ErrorMessage varchar(MAX)

			IF @Command='Insert'
				BEGIN
					
					BEGIN TRANSACTION

					BEGIN TRY 
						
						INSERT INTO tblUsersCommunication
						(
							UserId,
							MobileNo,
							EmailId
						)
						VALUES
						(
							@UserId,
							@MobileNo,
							@EmailId
						)

						
						COMMIT TRANSACTION
					END TRY 

					BEGIN CATCH 
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION

						RAISERROR(@ErrorMessage,16,1)
					END CATCH

				END
			ELSE IF @Command='Update'
				BEGIN 
					
					BEGIN TRANSACTION

					BEGIN TRY 
						
						SELECT 
						
						@MobileNo=CASE WHEN @MobileNo IS NULL THEN UC.MobileNo ELSE @MobileNo END,
						@EmailId=CASE WHEN @EmailId IS NULL THEN UC.EmailId ELSE @EmailId END
						FROM tblUsersCommunication AS UC 
							WHERE UC.UserId=@UserId

							UPDATE tblUsersCommunication
								SET 
									EmailId=@EmailId,
									MobileNo=@MobileNo
										WHERE UserId=@UserId

										COMMIT TRANSACTION

					END TRY 

					BEGIN CATCH 
					SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION

						RAISERROR(@ErrorMessage,16,1)

					END CATCH 

					

				END
			ELSE IF @Command='Delete'
				BEGIN
					BEGIN TRANSACTION

					BEGIN TRY 
						
						DELETE FROM tblUsersCommunication	
							WHERE UserId=@UserId

										COMMIT TRANSACTION

					END TRY 

					BEGIN CATCH 
					SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION

						RAISERROR(@ErrorMessage,16,1)

					END CATCH 

					
				END

	END 
