﻿CREATE PROCEDURE [dbo].[uspSetUsers]
	
	@UserId Numeric(18,0)=NULL,
	@FirstName Varchar(50)=NULL,
	@LastName varchar(50)=NULL,

	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT,
	@GetUserId Numeric(18,0) OUT
AS
	
		INSERT INTO tblUsers
		(
			FirstName,
			LastName
		)
		VALUES
		(
			@FirstName,
			@LastName
		)

		SET @Status=1
		SET @Message='Insert Succesfully'

		SET @GetUserId=@@IDENTITY

GO