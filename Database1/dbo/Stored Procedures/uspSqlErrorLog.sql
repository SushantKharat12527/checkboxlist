﻿CREATE PROC uspSqlErrorLog
(
	        @ErrorMessage varchar(MAX),
			@ErrorLineNo int,
			@ErrorNumber int,
			@ErrorProcedure Varchar(MAX),
			@ErrorState int
)
AS
	BEGIN 
		
		INSERT INTO AuditDB..tblSqlErrorLog
		(
			ErrorMessage,
			ErrorLineNo,
			ErrorNumber,
			ErrorProcedure,
			ErrorState
		)
		VALUES
		(
			@ErrorMessage,
			@ErrorLineNo,
			@ErrorNumber,
			@ErrorProcedure,
			@ErrorState
		)

	END 
