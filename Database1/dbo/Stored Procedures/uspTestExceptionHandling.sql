﻿CREATE PROC uspTestExceptionHandling
AS
	BEGIN 
		BEGIN TRY
		
		DECLARE @value int
			SET @value=2;

			BEGIN TRY 

				SELECT @value/0; -- Here i m predicate there will error come		

			END TRY 

			BEGIN CATCH
				--RAISERROR('Inner Exception Error Catch',16,1);
				SELECT @value/1 as 'Default Calculation'
			END CATCH

			SELECT @value AS 'Value'

	END TRY 

	BEGIN CATCH 
		
		DECLARE @ErrorMessage Varchar(MAX) = ERROR_MESSAGE()
		DECLARE @ErrorLine int=Error_LINE()
		DECLARE @ErrorNumber int=ERROR_NUMBER()
		DECLARE @ErrorProcedure Varchar(MAX)=ERROR_PROCEDURE()
		DECLARE @ErrorState int=ERROR_STATE()

		EXEC uspSqlErrorLog 
			@ErrorMessage,
			@ErrorLine,
			@ErrorNumber,
			@ErrorProcedure,
			@ErrorState
			

		RAISERROR(@ErrorMessage,16,1)
	END CATCH

	END
