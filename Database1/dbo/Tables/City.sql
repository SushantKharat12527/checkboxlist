﻿CREATE TABLE [dbo].[City] (
    [City_ID]   INT           NOT NULL,
    [City_Name] VARCHAR (MAX) NOT NULL,
    [State_ID]  INT           NOT NULL,
    FOREIGN KEY ([State_ID]) REFERENCES [dbo].[State] ([State_ID])
);

