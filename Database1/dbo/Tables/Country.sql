﻿CREATE TABLE [dbo].[Country] (
    [Country_ID]   INT           NOT NULL,
    [Country_Name] VARCHAR (MAX) NOT NULL,
    PRIMARY KEY CLUSTERED ([Country_ID] ASC)
);

