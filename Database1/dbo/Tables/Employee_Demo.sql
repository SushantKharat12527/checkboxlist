﻿CREATE TABLE [dbo].[Employee_Demo] (
    [Emp_ID]   INT             IDENTITY (1, 1) NOT NULL,
    [Emp_Name] VARCHAR (55)    NULL,
    [Emp_Sal]  DECIMAL (10, 2) NULL,
    PRIMARY KEY CLUSTERED ([Emp_ID] ASC)
);


GO
CREATE TRIGGER triBeforeInsert
ON Employee_Demo
INSTEAD OF INSERT
AS
	DECLARE @empid int
	DECLARE @empname varchar(55)
	DECLARE @empsal decimal(10,2)
	DECLARE @audit_action varchar(100)

	SELECT 
		@empid=i.Emp_ID,
		@empname=i.Emp_Name,
		@empsal=i.Emp_Sal
		FROM inserted AS I

		IF @empsal>=1000.00
			BEGIN

			SET @audit_action='Inserted Record'

				-- Store data in Physical Table
				INSERT INTO Employee_Demo
				(
					Emp_Name,
					Emp_Sal
				)
				VALUES
				(
					@empname,
					@empsal
				)

				-- Audit

				INSERT INTO AuditDB..Employee_Demo_Audit
				(
					Emp_ID,
					Emp_Name,
					Emp_Sal,
					Audit_Action,
					Audit_Timestamp
				)
				VALUES
				(
					@empid,
					@empname,
					@empsal,
					@audit_action,
					GetDate()
				)

			END
		ELSE
			BEGIN
				
				SET @audit_action='Transaction Failed. Salary is not grether than 1000.00'

				INSERT INTO AuditDB..Employee_Demo_Audit
				(
					Emp_ID,
					Emp_Name,
					Emp_Sal,
					Audit_Action,
					Audit_Timestamp
				)
				VALUES
				(
					@empid,
					@empname,
					@empsal,
					@audit_action,
					GetDate()
				)

			END 


GO
CREATE TRIGGER triBeforeUpdate
ON Employee_Demo
INSTEAD OF UPDATE
AS
	DECLARE @empid int
	DECLARE @empname varchar(55)
	DECLARE @empsal decimal(10,2)
	DECLARE @audit_action varchar(100)

	SELECT 
		@empid=i.Emp_ID,
		@empname=i.Emp_Name,
		@empsal=i.Emp_Sal
		FROM inserted AS I

		IF @empsal>=1000.00
			BEGIN

			SET @audit_action='Updated Record'

				-- Store data in Physical Table
				UPDATE Employee_Demo
					SET
						Emp_Name=@empname,
						Emp_Sal=@empsal
							WHERE Emp_ID=@empid

				-- Audit

				INSERT INTO AuditDB..Employee_Demo_Audit
				(
					Emp_ID,
					Emp_Name,
					Emp_Sal,
					Audit_Action,
					Audit_Timestamp
				)
				VALUES
				(
					@empid,
					@empname,
					@empsal,
					@audit_action,
					GetDate()
				)

			END
		ELSE
			BEGIN
				
				SET @audit_action='Transaction Failed On Update. Salary is not grether than 1000.00'

				INSERT INTO AuditDB..Employee_Demo_Audit
				(
					Emp_ID,
					Emp_Name,
					Emp_Sal,
					Audit_Action,
					Audit_Timestamp
				)
				VALUES
				(
					@empid,
					@empname,
					@empsal,
					@audit_action,
					GetDate()
				)

			END 


GO
CREATE TRIGGER triBeforeDelete
ON Employee_Demo
INSTEAD OF DELETE
AS
	DECLARE @empid int
	DECLARE @empname varchar(55)
	DECLARE @empsal decimal(10,2)
	DECLARE @audit_action varchar(100)

	SELECT 
		@empid=i.Emp_ID,
		@empname=i.Emp_Name,
		@empsal=i.Emp_Sal
		FROM deleted AS I

		
			SET @audit_action='Someone delete record.'


				-- Audit

				INSERT INTO AuditDB..Employee_Demo_Audit
				(
					Emp_ID,
					Emp_Name,
					Emp_Sal,
					Audit_Action,
					Audit_Timestamp
				)
				VALUES
				(
					@empid,
					@empname,
					@empsal,
					@audit_action,
					GetDate()
				)
			
