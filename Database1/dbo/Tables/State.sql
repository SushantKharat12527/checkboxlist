﻿CREATE TABLE [dbo].[State] (
    [State_ID]   INT           NOT NULL,
    [State_Name] VARCHAR (MAX) NOT NULL,
    [Country_ID] INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([State_ID] ASC),
    FOREIGN KEY ([Country_ID]) REFERENCES [dbo].[Country] ([Country_ID])
);

