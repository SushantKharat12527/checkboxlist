﻿CREATE TABLE [dbo].[tblAdmin] (
    [PersonID]  INT           NOT NULL,
    [FirstName] VARCHAR (255) NULL,
    [LastName]  VARCHAR (255) NULL,
    [Salary]    INT           NULL,
    PRIMARY KEY CLUSTERED ([PersonID] ASC)
);

