﻿CREATE TABLE [dbo].[tblAdminLogin] (
    [PersonID] INT           NOT NULL,
    [UserName] VARCHAR (255) NULL,
    [Password] VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([PersonID] ASC)
);

