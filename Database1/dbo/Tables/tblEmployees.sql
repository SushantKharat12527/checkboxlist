﻿CREATE TABLE [dbo].[tblEmployees] (
    [EmployeeId]  NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [FirstName]   VARCHAR (50) NULL,
    [LastName]    VARCHAR (50) NULL,
    [IsTerminate] BIT          NULL,
    CONSTRAINT [PK_tblEmployees] PRIMARY KEY CLUSTERED ([EmployeeId] ASC)
);

