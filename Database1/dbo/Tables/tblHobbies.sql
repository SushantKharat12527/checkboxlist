﻿CREATE TABLE [dbo].[tblHobbies] (
    [Hobby_ID]   INT           IDENTITY (1, 1) NOT NULL,
    [Hobby_Name] VARCHAR (255) NOT NULL,
    [Interested] BIT           NULL,
    [User_ID]    INT           NULL,
    FOREIGN KEY ([User_ID]) REFERENCES [dbo].[tblUserDemo] ([User_ID])
);

