﻿CREATE TABLE [dbo].[tblPerson] (
    [Person_Id] INT           NOT NULL,
    [FirstName] VARCHAR (255) NULL,
    [LastName]  VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([Person_Id] ASC)
);

