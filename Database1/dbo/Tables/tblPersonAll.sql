﻿CREATE TABLE [dbo].[tblPersonAll] (
    [PersonId]  NUMERIC (18) NOT NULL,
    [FirstName] VARCHAR (50) NULL,
    [LastName]  VARCHAR (50) NULL,
    [Salary]    MONEY        NULL,
    [Wages]     MONEY        NULL,
    CONSTRAINT [PK_tblPersonAll] PRIMARY KEY CLUSTERED ([PersonId] ASC)
);

