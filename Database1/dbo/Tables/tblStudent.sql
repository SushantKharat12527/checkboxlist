﻿CREATE TABLE [dbo].[tblStudent] (
    [StudentId] NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (50) NULL,
    [LastName]  VARCHAR (50) NULL,
    CONSTRAINT [PK_tblStudent] PRIMARY KEY CLUSTERED ([StudentId] ASC)
);

