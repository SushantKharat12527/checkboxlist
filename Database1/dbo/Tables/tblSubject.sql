﻿CREATE TABLE [dbo].[tblSubject] (
    [SubjectId]   NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [SubJectName] VARCHAR (50) NULL,
    CONSTRAINT [PK_tblSubject] PRIMARY KEY CLUSTERED ([SubjectId] ASC)
);

