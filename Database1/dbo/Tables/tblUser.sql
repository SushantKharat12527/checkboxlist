﻿CREATE TABLE [dbo].[tblUser] (
    [UserId]    INT           NULL,
    [FirstName] VARCHAR (MAX) NULL,
    [LastName]  VARCHAR (MAX) NULL,
    [Onsite]    BIT           NULL
);

