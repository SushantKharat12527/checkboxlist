﻿CREATE TABLE [dbo].[tblUserAll] (
    [UserId]    INT           NOT NULL,
    [FirstName] VARCHAR (255) NULL,
    [LastName]  VARCHAR (255) NULL,
    [UserName]  VARCHAR (255) NULL,
    [Password]  VARCHAR (255) NULL,
    [Email]     VARCHAR (255) NULL,
    [MobileNo]  VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([UserId] ASC)
);

