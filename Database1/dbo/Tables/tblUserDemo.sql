﻿CREATE TABLE [dbo].[tblUserDemo] (
    [User_ID]   INT           IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (255) NOT NULL,
    [LastName]  VARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([User_ID] ASC)
);

