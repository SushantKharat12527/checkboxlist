﻿CREATE TABLE [dbo].[tblUserLogin] (
    [UserId]   NUMERIC (18) NOT NULL,
    [UserName] VARCHAR (50) NULL,
    [Password] VARCHAR (50) NULL,
    [IsActive] BIT          NULL,
    CONSTRAINT [PK_tblUserLogin] PRIMARY KEY CLUSTERED ([UserId] ASC)
);

