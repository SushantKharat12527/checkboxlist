﻿CREATE TABLE [dbo].[tblUsersCommunication] (
    [UserId]   NUMERIC (18) NOT NULL,
    [MobileNo] VARCHAR (50) NULL,
    [EmailId]  VARCHAR (50) NULL,
    CONSTRAINT [PK_tblUsersCommunication] PRIMARY KEY CLUSTERED ([UserId] ASC)
);

