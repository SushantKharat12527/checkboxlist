﻿CREATE TABLE [dbo].[tblUsersCommunicationMany] (
    [UserCommunicationId] NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [MobileNo]            VARCHAR (50) NULL,
    [EmailId]             VARCHAR (50) NULL,
    [UserId]              NUMERIC (18) NULL,
    CONSTRAINT [PK_tblUsersCommunicationMany] PRIMARY KEY CLUSTERED ([UserCommunicationId] ASC)
);

