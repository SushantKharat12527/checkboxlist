﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="CheckBoxList_UserHobbies.aspx.cs" Inherits="Sol_CheckBoxListTask.CheckBoxList_UserHobbies" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        td{
            padding:10px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     
            <asp:ScriptManager ID="scriptManger" runat="server">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>
                    <table>
                <tr>
                    <td>
                            <asp:TextBox ID="txtFirstName" runat="server" PlaceHolder="First Name"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                            <asp:TextBox ID="txtLastName" runat="server" PlaceHolder="Last Name"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                            <asp:CheckBoxList ID="chkHobby" runat="server" RepeatColumns="1" RepeatLayout="Table">
                            </asp:CheckBoxList>
                    </td>
                </tr>
                 <tr>
                    <td>
                            <asp:Button ID="btnUpdate" runat="server" Text="Submit" OnClick="btnUpdate_Click"/>
                    </td>
                </tr>
                <tr>
                    <td>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
                </ContentTemplate>
            </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
