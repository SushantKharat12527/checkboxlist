﻿using Sol_CheckBoxListTask.Dal;
using Sol_CheckBoxListTask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_CheckBoxListTask
{
    public partial class CheckBoxList_UserHobbies : System.Web.UI.Page
    {

        #region Declaration
        private UserDal userDalObj = null;
        #endregion

        protected async void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
                await BindUserData();
        }

        #region Private Property
        private String FirstNameBinding
        {
            get
            {
                return txtFirstName.Text;
            }
            set
            {
                txtFirstName.Text = value;
            }
        }

        private String LastNameBinding
        {
            get
            {
                return txtLastName.Text;
            }
            set
            {
                txtLastName.Text = value;
            }
        }

        private List<UserHobbyEntity> HobbyBinding
        {
            get
            {
                return
                     chkHobby
                     ?.Items
                     ?.Cast<ListItem>()
                     ?.AsEnumerable()
                     ?.Select((leListItemObj) => new UserHobbyEntity()
                     {
                         HobbyId = Convert.ToDecimal(leListItemObj.Value),
                         HobbyName = leListItemObj.Text,
                         Intrested = leListItemObj.Selected
                     })
                     ?.ToList();

            }
            set
            {
                chkHobby.DataSource = value;
                chkHobby.DataTextField = "HobbyName";
                chkHobby.DataValueField = "HobbyId";
                chkHobby.DataBind();

                // Selected Property Bind
                foreach (ListItem lstObj in chkHobby.Items)
                {
                    lstObj.Selected = Convert.ToBoolean(
                            value
                            ?.AsEnumerable()
                            ?.Where((leUserentityObj) => leUserentityObj.HobbyName == lstObj.Text)
                            ?.FirstOrDefault()
                            ?.Intrested);
                }
            }
        }
        #endregion

        #region Private Methods
        private async Task BindUserData()
        {
            // Create an Insatnce of User Dal
            userDalObj = new UserDal();

            // get User Data from Dal 
            var getUserData = await userDalObj.GetUserData(new Models.UserEntity()
            {
                UserId = 1
            });

            //Binding
            await this.UserMappingEntityToUiData(getUserData);
        }

        /// <summary>
        /// This is for display Data in Ui Control
        /// </summary>
        /// <param name="userEntityObj"></param>
        /// <returns></returns>
        private async Task UserMappingEntityToUiData(UserEntity userEntityObj)
        {
            await Task.Run(() => {

                // Mapping
                this.FirstNameBinding = userEntityObj.FirstName;
                this.LastNameBinding = userEntityObj.LastName;

                this.HobbyBinding = userEntityObj.UserHobby;
            });
        }

        /// <summary>
        /// Get data from Ui to Entity
        /// </summary>
        /// <param name="userEntityObj"></param>
        /// <returns></returns>
        private async Task<UserEntity> UserMappingUiToEntityData()
        {
            return await Task.Run(() => {
                var userEntityObj = new UserEntity()
                {
                    UserId = 1,
                    FirstName = FirstNameBinding,
                    LastName = LastNameBinding,

                    UserHobby = HobbyBinding
                };

                return userEntityObj;
            });
        }
        #endregion

        protected async void btnUpdate_Click(object sender, EventArgs e)
        {
            // Mapping from UI to Entity
            var userEntityObj = await this.UserMappingUiToEntityData();

            // Call User Dal Object for Update


        }

    }
}