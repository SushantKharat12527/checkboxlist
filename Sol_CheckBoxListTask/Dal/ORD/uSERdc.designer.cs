﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sol_CheckBoxListTask.Dal.ORD
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="Testdb")]
	public partial class uSERdcDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    #endregion
		
		public uSERdcDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["TestdbConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public uSERdcDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public uSERdcDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public uSERdcDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public uSERdcDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<uspGetUsersResultSet> uspGetUsersResultSets
		{
			get
			{
				return this.GetTable<uspGetUsersResultSet>();
			}
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.uspGetUsers")]
		public ISingleResult<uspGetUsersResultSet> uspGetUsers([global::System.Data.Linq.Mapping.ParameterAttribute(Name="Command", DbType="VarChar(100)")] string command, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="UserId", DbType="Decimal(18,0)")] System.Nullable<decimal> userId)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), command, userId);
			return ((ISingleResult<uspGetUsersResultSet>)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.uspSetUserHobby")]
		public int uspSetUserHobby([global::System.Data.Linq.Mapping.ParameterAttribute(Name="HobbyId", DbType="Decimal(18,0)")] System.Nullable<decimal> hobbyId, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="HobbyName", DbType="VarChar(50)")] string hobbyName, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Intrested", DbType="Bit")] System.Nullable<bool> intrested, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="UserId", DbType="Decimal(18,0)")] System.Nullable<decimal> userId, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Status", DbType="Int")] ref System.Nullable<int> status, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Message", DbType="VarChar(MAX)")] ref string message)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), hobbyId, hobbyName, intrested, userId, status, message);
			status = ((System.Nullable<int>)(result.GetParameterValue(4)));
			message = ((string)(result.GetParameterValue(5)));
			return ((int)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.uspSetUsers")]
		public int uspSetUsers([global::System.Data.Linq.Mapping.ParameterAttribute(Name="UserId", DbType="Decimal(18,0)")] System.Nullable<decimal> userId, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="FirstName", DbType="VarChar(50)")] string firstName, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="LastName", DbType="VarChar(50)")] string lastName, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Status", DbType="Int")] ref System.Nullable<int> status, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="Message", DbType="VarChar(MAX)")] ref string message, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="GetUserId", DbType="Decimal(18,0)")] ref System.Nullable<decimal> getUserId)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), userId, firstName, lastName, status, message, getUserId);
			status = ((System.Nullable<int>)(result.GetParameterValue(3)));
			message = ((string)(result.GetParameterValue(4)));
			getUserId = ((System.Nullable<decimal>)(result.GetParameterValue(5)));
			return ((int)(result.ReturnValue));
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="")]
	public partial class uspGetUsersResultSet
	{
		
		private System.Nullable<decimal> _UserId;
		
		private string _FirstName;
		
		private string _LastName;
		
		private System.Nullable<decimal> _HobbyId;
		
		private string _HobbyName;
		
		private System.Nullable<bool> _Intrested;
		
		public uspGetUsersResultSet()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UserId")]
		public System.Nullable<decimal> UserId
		{
			get
			{
				return this._UserId;
			}
			set
			{
				if ((this._UserId != value))
				{
					this._UserId = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_FirstName")]
		public string FirstName
		{
			get
			{
				return this._FirstName;
			}
			set
			{
				if ((this._FirstName != value))
				{
					this._FirstName = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_LastName")]
		public string LastName
		{
			get
			{
				return this._LastName;
			}
			set
			{
				if ((this._LastName != value))
				{
					this._LastName = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_HobbyId")]
		public System.Nullable<decimal> HobbyId
		{
			get
			{
				return this._HobbyId;
			}
			set
			{
				if ((this._HobbyId != value))
				{
					this._HobbyId = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_HobbyName")]
		public string HobbyName
		{
			get
			{
				return this._HobbyName;
			}
			set
			{
				if ((this._HobbyName != value))
				{
					this._HobbyName = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Intrested")]
		public System.Nullable<bool> Intrested
		{
			get
			{
				return this._Intrested;
			}
			set
			{
				if ((this._Intrested != value))
				{
					this._Intrested = value;
				}
			}
		}
	}
}
#pragma warning restore 1591
