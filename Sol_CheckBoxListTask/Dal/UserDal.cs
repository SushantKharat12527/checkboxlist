﻿using Sol_CheckBoxListTask.Dal.ORD;
using Sol_CheckBoxListTask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;

namespace Sol_CheckBoxListTask.Dal
{
    public class UserDal
    {
        #region Declaration
        private uSERdcDataContext _dc = null;
        #endregion

        #region Constructor
        public UserDal()
        {
            _dc = new uSERdcDataContext();
        }
        #endregion

        #region Public Methods
        public async Task<dynamic> AddUserData(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;
            decimal? getUserId = null;
            try
            {

                return await Task.Run(async () => {
                    // Store First User Info.
                    var data = _dc
                            ?.uspSetUsers(
                                    userEntityObj?.UserId,
                                    userEntityObj?.FirstName,
                                    userEntityObj?.LastName,
                                    ref status,
                                    ref message,
                                    ref getUserId
                                );

                    // Bind Id to Existing user Entity Data
                    userEntityObj.UserId = getUserId;

                    // add Hobby Data
                    await this.AddUserHobbyData(userEntityObj);

                    return message;
                });


            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<UserEntity> GetUserData(UserEntity userEntityObj)
        {
            return await Task.Run(() => {

                return

                  _dc
                  ?.uspGetUsers(
                      "UserData",
                      userEntityObj.UserId
                      )
                  ?.AsEnumerable()
                  ?.Select(
                        (leUspGetUsersResultSetObj) => new UserEntity()
                        {
                            UserId = leUspGetUsersResultSetObj.UserId,
                            FirstName = leUspGetUsersResultSetObj.FirstName,
                            LastName = leUspGetUsersResultSetObj.LastName,

                            UserHobby = (this.GetUserHobbyData(new UserHobbyEntity()
                            {
                                UserId = leUspGetUsersResultSetObj.UserId
                            })).Result as List<UserHobbyEntity>
                        })

                    ?.ToList()
                    ?.FirstOrDefault();



            });
        }
        #endregion

        #region Private Property
        private Func<uspGetUsersResultSet, UserHobbyEntity> SelectUserHobbyData
        {
            get
            {
                return
                       (leUspGetUsersResultSetObj) => new UserHobbyEntity()
                       {
                           HobbyId = leUspGetUsersResultSetObj.HobbyId,
                           HobbyName = leUspGetUsersResultSetObj.HobbyName,
                           Intrested = leUspGetUsersResultSetObj.Intrested
                       };
            }
        }



        #endregion

        #region Private Methods
        private async Task AddUserHobbyData(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;

            await Task.Run(() => {

                foreach (UserHobbyEntity userHobbyEntityObj in userEntityObj.UserHobby)
                {
                    _dc
                    ?.uspSetUserHobby(
                        userHobbyEntityObj.HobbyId,
                        userHobbyEntityObj.HobbyName,
                        userHobbyEntityObj.Intrested,
                        userEntityObj.UserId,
                        ref status,
                        ref message
                        );
                }


            });
        }

        private async Task<IEnumerable<UserHobbyEntity>> GetUserHobbyData(UserHobbyEntity userHobbyEntityObj)
        {
            return await Task.Run(() => {

                return
                    _dc
                    ?.uspGetUsers(
                        "UserHobbyData",
                        userHobbyEntityObj.UserId
                        )
                    ?.AsEnumerable()
                    ?.Select(this.SelectUserHobbyData)
                    ?.ToList();

            });
        }


        #endregion

    }
}