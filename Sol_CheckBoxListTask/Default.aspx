﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sol_CheckBoxListTask.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <table>
                <tr>
                    <td>
                            <asp:TextBox ID="txtFirstName" runat="server" PlaceHolder="First Name"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                            <asp:TextBox ID="txtLastName" runat="server" PlaceHolder="Last Name"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                            <asp:CheckBoxList ID="chkHobby" runat="server" RepeatColumns="1" RepeatLayout="Table">
                                <asp:ListItem Text="Playing Cricket" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="Book Reading" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="Dancing" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="Cooking" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="Drawing" Selected="False"></asp:ListItem>
                            </asp:CheckBoxList>
                    </td>
                </tr>
                 <tr>
                    <td>
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
    </div>
    </form>
</body>
</html>
