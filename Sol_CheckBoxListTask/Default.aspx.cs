﻿using Sol_CheckBoxListTask.Dal;
using Sol_CheckBoxListTask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_CheckBoxListTask
{
    public partial class Default : System.Web.UI.Page
    {
        #region Declaration
        private UserDal userDalObj = null;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        #region Private Property
        private String BindLabelMessage
        {
            get
            {
                return lblMessage.Text;
            }
            set
            {
                lblMessage.Text = value;
            }

        }

        private List<UserHobbyEntity> GetHobbyData
        {
            get
            {
                //List<UserHobbyEntity> listUserHobbyEntityObj = new List<UserHobbyEntity>();
                //foreach(ListItem listItemObj in chkHobby.Items)
                //{
                //    listUserHobbyEntityObj.Add(new UserHobbyEntity()
                //    {
                //        HobbyName = listItemObj.Text, // get Hobby Name
                //        Intrested = listItemObj.Selected // get Hobby Selected
                //    });
                //}

                //return listUserHobbyEntityObj;

                return chkHobby
                    ?.Items
                    ?.Cast<ListItem>()
                    ?.AsEnumerable()
                    ?.Select((leListCheckBoxObj) => new UserHobbyEntity()
                    {
                        HobbyName = leListCheckBoxObj.Text, // get Hobby Name
                        Intrested = leListCheckBoxObj.Selected // get Hobby Selected
                    })
                    ?.ToList();




            }
        }
        #endregion 

        #region Private Methods
        private async Task<UserEntity> UserMappingData()
        {
            return await Task.Run(() => {
                var userEntityObj = new UserEntity()
                {
                    FirstName = txtFirstName.Text,
                    LastName = txtLastName.Text,
                    UserHobby = this.GetHobbyData

                };

                return userEntityObj;
            });
        }

        private async Task AddUserData(UserEntity userEntityObj)
        {
            // Create an instance of User Dal.
            userDalObj = new UserDal();

            // add Data to Database and Bind to the lable
            this.BindLabelMessage = await userDalObj.AddUserData(userEntityObj);
        }
        #endregion 

        protected async void btnSubmit_Click(object sender, EventArgs e)
        {
            // Mapping User Entity Obj.
            var userEntityObj = await this.UserMappingData();

            // Add data
            await AddUserData(userEntityObj);
        }

    }
}