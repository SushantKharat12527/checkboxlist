﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_CheckBoxListTask.Models
{
    public class UserEntity
    {
      public decimal? UserId { get; set; }
        public String FirstName  { get; set; }
        public String LastName { get; set; }
        public List<UserHobbyEntity> UserHobby { get; set; }
    }
}