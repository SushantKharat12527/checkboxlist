﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_CheckBoxListTask.Models
{
    public class UserHobbyEntity
    {

        public decimal? UserId { get; set; }
        public decimal? HobbyId { get; set; }

        public String HobbyName { get; set; }

        public bool? Intrested { get; set; }

    }
}